ARG PHP_VERSION=${PHP_VERSION:-7.4.2}

FROM php:${PHP_VERSION}-fpm-alpine AS backend-setup

# Install system dependencies
RUN apk update && apk add dcron busybox-suid curl libcap zip unzip git php-pcntl

# Install PHP extensions
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/
RUN install-php-extensions zip pdo_pgsql pcntl
RUN install-php-extensions gd

# Install composer
COPY --from=composer/composer:2 /usr/bin/composer /usr/local/bin/composer

# Set working directory
ENV LARAVEL_PATH=/var/www/html
WORKDIR $LARAVEL_PATH

# Add non-root user: 'app'
ARG NON_ROOT_GROUP=${NON_ROOT_GROUP:-app}
ARG NON_ROOT_USER=${NON_ROOT_USER:-app}
ARG PUID=${PUID:-100}
ARG PGID=${PGID:-101}

ENV NON_ROOT_GROUP=$NON_ROOT_GROUP
ENV NON_ROOT_USER=$NON_ROOT_USER

RUN addgroup -g $PGID -S $NON_ROOT_GROUP && adduser -S $NON_ROOT_USER -G $NON_ROOT_GROUP -u $PUID
RUN addgroup $NON_ROOT_USER wheel

FROM backend-setup as frontend-setup

RUN apk add --update nodejs npm

RUN npm i -g yarn

RUN apk add --no-cache python2

RUN apk add python3 build-base make g++ bash grep

# Needed for prerenderings
RUN apk add chromium

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
ENV PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser

FROM frontend-setup as test-app

ARG TESTING=false

# Copy app
COPY --chown=$NON_ROOT_USER:$NON_ROOT_GROUP . $LARAVEL_PATH/
COPY ./docker/config/php/local.ini /usr/local/etc/php/conf.d/local.ini
COPY .env.testing .env

RUN if [ ${TESTING} = true ]; then \
    yarn && \
    composer install \
  ;fi

COPY ./docker/scripts/entrypoint.test.sh /
ENTRYPOINT ["sh", "/entrypoint.test.sh"]

FROM frontend-setup AS server-setup

# Install supervisord implementation
COPY --from=ochinchina/supervisord:latest /usr/local/bin/supervisord /usr/local/bin/supervisord

# Install caddy
COPY --from=caddy:2.2.1 /usr/bin/caddy /usr/local/bin/caddy
RUN setcap 'cap_net_bind_service=+ep' /usr/local/bin/caddy

# Set cron job
COPY ./docker/config/crontab /etc/crontabs/$NON_ROOT_USER
RUN chmod 777 /usr/sbin/crond
RUN chown -R $NON_ROOT_USER:$NON_ROOT_GROUP /etc/crontabs/$NON_ROOT_USER && setcap cap_setgid=ep /usr/sbin/crond

# Switch to non-root 'app' user & install app dependencies
RUN chown -R $NON_ROOT_USER:$NON_ROOT_GROUP $LARAVEL_PATH
USER $NON_ROOT_USER

# Copy app
COPY --chown=$NON_ROOT_USER:$NON_ROOT_GROUP . $LARAVEL_PATH/
COPY ./docker/config/php/local.ini /usr/local/etc/php/conf.d/local.ini

FROM server-setup as dev-app

COPY ./docker/scripts/entrypoint.dev.sh /
ENTRYPOINT ["sh", "/entrypoint.dev.sh"]

FROM server-setup AS prod-app

COPY package.json ./
COPY yarn.lock ./

RUN yarn

COPY . .

RUN yarn build:web

COPY composer.* ./

RUN composer install --prefer-dist --no-scripts --no-dev --no-autoloader
RUN rm -rf /home/$NON_ROOT_USER/.composer

COPY ./docker/scripts/entrypoint.prod.sh /
ENTRYPOINT ["sh", "/entrypoint.prod.sh"]

# Set any ENVs

ARG APP_NAME=${APP_NAME}
ARG APP_ENV=${APP_ENV}
ARG APP_KEY=${APP_KEY}
ARG APP_DEBUG=${APP_DEBUG}
ARG APP_URL=${APP_URL}
ARG BASE_URL=${APP_URL}
ARG BACKEND_URL=${BACKEND_URL}
ARG FRONTEND_APP_URL=${FRONTEND_APP_URL}
ARG FRONTEND_WEBSITE_URL=${FRONTEND_WEBSITE_URL}
ARG LOG_CHANNEL=${LOG_CHANNEL}
ARG LOG_GRAYLOG_HOST=${LOG_GRAYLOG_HOST}
ARG LOG_GRAYLOG_PORT=${LOG_GRAYLOG_PORT}
ARG DATABASE_URL=${DATABASE_URL}
ARG DB_CONNECTION=${DB_CONNECTION}
ARG DB_HOST=${DB_HOST}
ARG DB_PORT=${DB_PORT}
ARG DB_DATABASE=${DB_DATABASE}
ARG DB_USERNAME=${DB_USERNAME}
ARG DB_PASSWORD=${DB_PASSWORD}
ARG BROADCAST_DRIVER=${BROADCAST_DRIVER}
ARG CACHE_DRIVER=${CACHE_DRIVER}
ARG QUEUE_CONNECTION=${QUEUE_CONNECTION}
ARG SESSION_DRIVER=${SESSION_DRIVER}
ARG SESSION_LIFETIME=${SESSION_LIFETIME}
ARG REDIS_URL=${REDIS_URL}
ARG REDIS_HOST=${REDIS_HOST}
ARG REDIS_PASSWORD=${REDIS_PASSWORD}
ARG REDIS_PORT=${REDIS_PORT}
ARG MAIL_MAILER=${MAIL_MAILER}
ARG MAIL_REPLY_TO=${MAIL_REPLY_TO}
ARG MAIL_HOST=${MAIL_HOST}
ARG MAIL_PORT=${MAIL_PORT}
ARG MAIL_USERNAME=${MAIL_USERNAME}
ARG MAIL_PASSWORD=${MAIL_PASSWORD}
ARG MAIL_ENCRYPTION=${MAIL_ENCRYPTION}
ARG MAIL_FROM_ADDRESS=${MAIL_FROM_ADDRESS}
ARG MAIL_FROM_NAME=${MAIL_FROM_NAME}
ARG SUPPORT_MAIL_ADDRESS=${SUPPORT_MAIL_ADDRESS}
ARG DIRECTOR_MAIL_ADDRESS=${DIRECTOR_MAIL_ADDRESS}
ARG DIRECTOR_NAME=${DIRECTOR_NAME}
ARG AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
ARG AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
ARG AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}
ARG AWS_BUCKET=${AWS_BUCKET}
ARG AWS_ENDPOINT=${AWS_ENDPOINT}
ARG PUSHER_APP_ID=${PUSHER_APP_ID}
ARG PUSHER_APP_KEY=${PUSHER_APP_KEY}
ARG PUSHER_APP_SECRET=${PUSHER_APP_SECRET}
ARG PUSHER_APP_CLUSTER=${PUSHER_APP_CLUSTER}
ARG MIX_PUSHER_APP_KEY=${MIX_PUSHER_APP_KEY}
ARG MIX_PUSHER_APP_CLUSTER=${MIX_PUSHER_APP_CLUSTER}
ARG SANCTUM_STATEFUL_DOMAINS=${SANCTUM_STATEFUL_DOMAINS}
ARG CORS_DOMAINS=${SANCTUM_STATEFUL_DOMAINS}
ARG RECAPTCHA_KEY=${RECAPTCHA_KEY}
ARG TELESCOPE_PATH=${TELESCOPE_PATH}
ARG HORIZON_PATH=${HORIZON_PATH}
ARG SCOUT_DRIVER=${SCOUT_DRIVER}
ARG SCOUT_QUEUE=${SCOUT_QUEUE}
ARG MEILISEARCH_HOST=${MEILISEARCH_HOST}
ARG MEILISEARCH_KEY=${MEILISEARCH_KEY}
