<?php

return [
  'auth' => [
    /*
      |--------------------------------------------------------------------------
      | Auto Register
      |--------------------------------------------------------------------------
      |
      | Automatically register a new user with the provided login credentials
      | if the user doesn't exist.
      |
      */
    'auto_register' => false,
  ],

  'frontend' => [
    'url' => env('FRONTEND_APP_URL'),
  ]
];
