import { studly } from "src/tools"

export const getMiddleware = (path) => {
  const [moduleName, name] = path.split('.')

  if (!moduleName || !name) {
    throw new Error('getMiddleware parameter must be in format "moduleName.middlewareName"')
  }

  return require(`module/${studly(moduleName, true)}/Frontend/router/middlewares`)[name]
}

export const chainMiddlewares = (middlewares) => {
  return (to, from, next) => {
    const localMiddlewares = [...middlewares]

    const doNext = val => {
      if (!localMiddlewares.length || val === false || (typeof val === 'object' && val !== null)) {
        return next(val)
      }

      localMiddlewares.shift()(to, from, doNext)
    }

    doNext()
  }
}
