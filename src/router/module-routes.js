import { kebab } from 'src/tools'
import enabledModules from 'module/enabled.json'

const routes = []

const updateRoute = (route, moduleName) => {
  const modulePath = `/${kebab(moduleName)}`

  if (route.path && !route.path.startsWith(modulePath)) {
    const path = route.path

    route.path = modulePath

    if (!path.startsWith('/')) {
      route.path += '/'
    }

    route.path += path
  } else if (!route.path) {
    route.path = modulePath
  }

  return route
}

enabledModules.forEach(moduleName => {
  try {
    const route = require(`module/${moduleName}/Frontend/router`).default

    if (Array.isArray(route)) {
      route.forEach(rte => routes.push(updateRoute(rte, moduleName)))
    }
    if (Object.keys(route).length) {
      routes.push(updateRoute(route, moduleName))
    }
  } catch (e) { }
})

export default routes
