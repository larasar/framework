import moduleRoutes from './module-routes'
import Reroute from 'pages/Reroute'

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: 'main.index',
        path: '',
        component: () => import('pages/Index.vue')
      },

      // Do not remove
      {
        name: 'reroute',
        path: 'view',
        component: Reroute
      }
    ]
  },

  ...moduleRoutes,

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
