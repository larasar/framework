import { boot } from 'quasar/wrappers'
import VuravelValidation from '@ezraobiwale/vuravel-validation'

export default boot(async ({ app }) => {
  app.use(VuravelValidation)
})
