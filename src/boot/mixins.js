import { boot } from 'quasar/wrappers'
import { isLoggedIn } from 'tools/auth'
import * as tools from 'tools'

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async ({ app }) => {
  app.mixin({
    methods: {
      ...tools,
      isLoggedIn
    }
  })
})
