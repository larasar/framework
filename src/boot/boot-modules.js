import { boot } from 'quasar/wrappers'
import enabledModules from 'module/enabled.json'
import * as config from 'module/config'

export default boot(async obj => {
  obj.app.config.globalProperties.$config = config

  enabledModules.forEach(async module => {
    try {
      const bootModule = require(`module/${module}/Frontend/boot`).default

      await bootModule(obj)
    } catch (e) { }
  })
})
