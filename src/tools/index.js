import { Notify, Screen } from 'quasar'

let dismissFns = {}

export const clearNotifications = message => {
  if (message && dismissFns[message]) {
    dismissFns[message]()

    delete dismissFns[message]

    return
  }

  Object.values(dismissFns).forEach(dismiss => dismiss())
  dismissFns = {}
}

export const isMobile = () => {
  return Screen.lt.md
}

export const kebab = (str) => `${str[0].toLowerCase()}${str.substr(1)}`.replace(/[A-Z]/g, chr => `-${chr.toLowerCase()}`)

export const notify = (
  message,
  color = 'positive',
  timeout = 5000,
  closeBtn = null
) => {
  const icons = {
    positive: 'check_circle',
    negative: 'error',
    warning: 'warning'
  }

  let closeBtnAction = {
    label: typeof closeBtn === 'string' ? closeBtn : trans('OK'),
    color: 'white',
    handler: () => clearNotifications(message)
  }

  if (typeof closeBtn === 'object') {
    closeBtnAction = {
      ...closeBtnAction,
      ...closeBtn
    }
  }

  dismissFns[message] = Notify.create({
    actions: [closeBtnAction],
    color: color,
    html: true,
    icon: icons[color] || 'info',
    message,
    position: isMobile() ? 'bottom' : 'bottom-left',
    progress: true,
    onDismiss: () => clearNotifications(message),
    timeout
  })

  return dismissFns[message]
}

export const requestError = resp => {
  let message = trans('Something went wrong')

  if (resp) {
    message = resp.message || resp.data.message || ''

    if (resp.errors || resp.data.errors) {
      message += '<ul>'

      Object.values(resp.errors || resp.data.errors).forEach(error => (message += `<li>${Array.isArray(error) ? error[0] : error}</li>`))

      message += '</ul>'
    }
  }

  notify(message, 'negative')
}

export const studly = (str, capitalise = false) => {
  const studlyStr = `${str}`.replace(/-[a-z]/gi, chr => chr[1].toUpperCase())

  if (!capitalise) {
    return studlyStr
  }

  return studlyStr[0].toUpperCase() + studlyStr.substring(1)
}

export const trans = (str, placeholders = {}) => {
  for (let key in placeholders) {
    str = str.replace(new RegExp(`:${key}`, 'g'), placeholders[key])
  }

  return str
}

export const tryCatch = async (tryFunc, catchFunc, notify = true) => {
  try {
    if (typeof tryFunc === 'function') {
      return await tryFunc()
    }
  } catch (error) {
    if (notify && error) {
      requestError(error)
    }

    if (typeof catchFunc === 'function') {
      return catchFunc(error)
    }
  }
}
