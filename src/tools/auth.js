import { Cookies } from 'quasar'

export const jwtStorageKey = process.env.JWT_STORAGE_KEY || 'jsk'

export const isLoggedIn = () => {
  Cookies.has(jwtStorageKey)
}
