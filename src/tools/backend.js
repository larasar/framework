import { Cookies } from 'quasar'
import { kebab, trans } from './index'
import { isLoggedIn, jwtStorageKey } from './auth'
import enabledModules from 'module/enabled.json'
import axios from 'axios'

const cookieOptions = { path: '/', expires: 10 }

const backend = axios.create({
  baseURL: process.env.BASE_URL,
  withCredentials: true
})

enabledModules.forEach((module) => {
  backend[module] = axios.create({
    baseURL: process.env.BACKEND_URL + `/${kebab(module)}`,
    withCredentials: true
  })

  backend[module].interceptors.request.use((config) => {
    if (isLoggedIn()) {
      config.headers.Authorization = `Bearer ${Cookies.get(jwtStorageKey)}`
    }

    return config
  }, error => Promise.reject(error))

  backend[module].interceptors.response.use(
    ({ data }) => data,
    ({ request, response, message }) => {
      let error = { message }

      if (response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx

        const { data, status } = response

        error = { data, status }
      } else if (request) {
        // The request was made but no response was received
        // `request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js

        error = { message: trans('No response received') };
      }

      return Promise.reject(error)
    }
  )
})

export const setToken = token => {
  Cookies.set(jwtStorageKey, token, cookieOptions)
}

export const removeToken = () => {
  Cookies.remove(jwtStorageKey, cookieOptions)
}

export default backend
