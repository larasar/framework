# Larasar

> A monolithic skeleton comprising Laravel and Quasar (Vue).

## Components

Name    |     Url
--------|-------------
Laravel | https://laravel.com
Quasar  | https://quasar.dev

## Idea

The idea is to have both Quasar and Laravel running together in the same repository while modularizing the parts. The modules are kept as composer libraries and can be installed independently into this skeleton.

## Prerequisites

- [Docker](https://docs.docker.com/engine/install/)
- [Docker compose](https://docs.docker.com/compose/install/)

## Setting up development

- `git clone -o larasar https://gitlab.com/larasar/framework [directory name]`
- `cp .env.example .env`
- Update variable values in `.env` file
- `COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose build app`
- `docker-compose up -d app`

> `COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1` is needed for multi-stage Dockerfile to work.

This builds and starts up the app, which is daemon supervisor which starts

- php-fpm
- laravel
- quasar
- cron
- horizon
- websockets

When you've set up your own code repository, run:

- `git remote add origin path/to/your/repository.git`
- `git push -u origin --all`

> This ensures your default remote is your repository and not [Larasar's](https://gitlab.com/larasar/framework).

### Docker compser services

- `app` - The project itself
- `test` - A slim version of the project to be used in CI/CD
- [Minio](https://min.io/) - For S3-compatible object storage
- [MailHog](https://github.com/mailhog/MailHog) - An email testing tool
- [Redis](https://redis.io/) - For caching and queues
- [Postgres](https://www.postgresql.org/) - The database
- [Meilisearch](https://www.meilisearch.com/) - A fast search-engine
- [Adminer](https://www.adminer.org/) - Database management GUI

> Since you don't need the `test` container running locally, better to speciry the `app` when starting containers: `docker-compose up -d app`.

> The `app` container depends on, and starts, all the other containers except `test` and `adminer`.

### Default Larasar libraries

By default, the framework includes the following larasar packages and module:

- [larasar/module-commands](https://gitlab.com/larasar/module-commands)
- [larasar/helpers](https://gitlab.com/larasar/helpers)
- [larasar/auth-module](https://gitlab.com/larasar/auth-module)

### Added Laravel libraries

- [laravel/horizon](https://laravel.com/docs/8.x/horizon)
- [laravel/scout](https://laravel.com/docs/8.x/scout)
- [laravel/telescope](https://laravel.com/docs/8.x/telescope)
- [laravel/tinker](https://laravel.com/docs/8.x/tinker)

> Laravel Scout is configured to work with Meilisearch by default.

### Other libraries

- [beyondcode/laravel-websockets](https://github.com/beyondcode/laravel-websockets)
- [d-scribe/laraquick](https://github.com/ezra-obiwale/laraquick)
- [predis/predis](https://github.com/predis/predis)
- [pusher/pusher-php-server](https://github.com/pusher/pusher-http-php)

### Creating a module

Use the commands provided by [larasar/module-commands](https://gitlab.com/larasar/module-commands) to create, package and publish your modules.

> Add the module name to `module/enabled.json` to enable it. Alternatively, you could call `php artisan status -e ModuleName`. Module `Auth` is enabled by default.

### Frontend configuration

Provide configurations in `module/config.js` and it's accessible by `this.$config.xyz` in the Quasar app.

For example, `this.$config.defaultRoute` points to the `defaultRoute` config in the `module/config.js`.

> `defualtRoute` indicates the route to navigate to after authentication. It can also be navigated to for other reasons.

## Setting up CI/CD test

Ensure your `.env.testing` file is properly configured and add the following to your scripts:

- `copy .env.testing .env`
- `COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up -d test`
- `docker-compose exec -T test php artisan test`
- `docker-compose exec -T test yarn test`
- `docker-compose down`

> You may add any other tests you need to perform before the last command

## Setting up production

Docker compose is not required in production. The `Dockerfile` is all that's needed. So, simply spin it up your usual way and:

- the production-built PWA frontend is available on port `80`
- the laravel backend is available on port `8080`
