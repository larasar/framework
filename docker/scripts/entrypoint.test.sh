#!/bin/bash

echo "🎬 entrypoint.test.sh: [$(whoami)] [PHP $(php -r 'echo phpversion();')]"

echo "🎬 artisan commands"

php artisan migrate --no-interaction --force
php artisan module:migrate --no-interaction --force
php artisan storage:link

php-fpm
