#!/bin/bash

echo "🎬 entrypoint.prod.sh: [$(whoami)] [PHP $(php -r 'echo phpversion();')]"

echo "🎬 artisan commands"

php artisan migrate --no-interaction --force
php artisan module:migrate --no-interaction --force
php artisan storage:link
php artisan config:cache
php artisan route:cache
php artisan view:cache

composer dump-autoload --no-interaction --no-dev --optimize

echo "🎬 start supervisord"
supervisord -c $LARAVEL_PATH/docker/config/prod/supervisor.conf
