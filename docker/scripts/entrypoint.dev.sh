#!/bin/bash

echo "🎬 entrypoint.dev.sh: [$(whoami)] [PHP $(php -r 'echo phpversion();')]"

composer i
yarn install

echo "🎬 artisan commands"

php artisan migrate --no-interaction --force
php artisan module:migrate --no-interaction --force
php artisan storage:link

composer dump-autoload --no-interaction

echo "🎬 start supervisord"
supervisord -c $LARAVEL_PATH/docker/config/dev/supervisor.conf
