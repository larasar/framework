<?php

namespace App\Console;

use Illuminate\Console\Application as Artisan;
use Illuminate\Console\Command;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ReflectionClass;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class Kernel extends ConsoleKernel
{
  /**
   * The Artisan commands provided by your application.
   *
   * @var array
   */
  protected $commands = [];

  /**
   * Define the application's command schedule.
   *
   * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
   * @return void
   */
  protected function schedule(Schedule $schedule)
  {
    // $schedule->command('inspire')->hourly();
  }

  /**
   * Register the commands for the application.
   *
   * @return void
   */
  protected function commands()
  {
    $this->load(__DIR__ . '/Commands');

    $this->loadLarasarModuleCommands();

    require base_path('routes/console.php');
  }

  protected function load($paths)
  {
    $paths = array_unique(Arr::wrap($paths));

    $paths = array_filter($paths, function ($path) {
      return is_dir($path);
    });

    if (empty($paths)) {
      return;
    }

    $appNamespace = $this->app->getNamespace();

    $moduleNamespace = 'Module\\';

    foreach ((new Finder)->in($paths)->files() as $command) {
      $command = $this->getFullyQualifiedCommandClassName($command, $appNamespace, $moduleNamespace);

      if (
        is_subclass_of($command, Command::class) &&
        !Str::contains($command, '.') &&
        !(new ReflectionClass($command))->isAbstract()
      ) {
        Artisan::starting(function ($artisan) use ($command) {
          $artisan->resolve($command);
        });
      }
    }
  }

  private function getFullyQualifiedCommandClassName(SplFileInfo $command, string $appNamespace, string $moduleNamespace): string
  {
    $commandRealPath = $command->getRealPath();
    $appRealPath = realpath(app_path());
    $moduleRealPath = realpath(larasar_module_path());

    $namespace = $appNamespace;
    $realPath = $appRealPath;

    if (Str::contains($commandRealPath, $moduleRealPath)) {
      $namespace = $moduleNamespace;
      $realPath = $moduleRealPath;
    }

    $command = $namespace . str_replace(
      ['/', '.php'],
      ['\\', ''],
      Str::after($commandRealPath, $realPath . DIRECTORY_SEPARATOR)
    );

    return $command;
  }

  private function loadLarasarModuleCommands()
  {
    $larasarModules = larasar_get_enabled_modules();

    foreach ($larasarModules as $moduleName) {
      $path = "${moduleName}/Backend/Console/Commands";

      if (!Storage::disk('module')->exists($path)) {
        continue;
      }

      $this->load(larasar_module_path($path));
    }
  }
}
