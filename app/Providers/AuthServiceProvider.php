<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
  /**
   * The policy mappings for the application.
   *
   * @var array
   */
  protected $policies = [
    // 'App\Models\Model' => 'App\Policies\ModelPolicy',
  ];

  /**
   * Register any authentication / authorization services.
   *
   * @return void
   */
  public function boot()
  {
    $this->registerPolicies();
  }

  public function policies()
  {
    $policies = $this->policies;

    larasar_each_enabled_backend_module(function ($moduleName) use (&$policies) {
      $policies = array_merge($policies, include(larasar_module_path("{$moduleName}/Backend/authPolicies.php")));
    });

    return $policies;
  }
}
